using System;

namespace OverEngineeredPong.Collections;

struct BitVector<T> where T : const int
{
	struct BitInfo
	{
		public int ArrayIndex;
		public uint64 BitMask;
		public uint64 BitIndex;

		public this(int absoluteIndex)
		{
			int32 bitSize = typeof(uint64).BitSize;
			float floatAbsIndex = (float)absoluteIndex;
			float floatBits = (float) bitSize;
			ArrayIndex = ((int) Math.Floor(floatAbsIndex / floatBits));
			BitIndex = (((uint64) absoluteIndex) - (((uint64) ArrayIndex) * ((uint64) bitSize)));
			BitMask = 1 << BitIndex;
		}
	}

	[Comptime, OnCompile(.TypeInit)]
	static void GenerateBitVector()
	{
		float floatT = (float)T;
		float floatBits = (float) typeof(uint64).BitSize;
		int numElements = (int) Math.Ceiling(floatT / floatBits);
		Compiler.EmitTypeBody(typeof(Self), scope $"""
			uint64[{numElements}] mBits;
			""");
	}

	public bool this[int bitIndex]
	{
		get
		{
			Runtime.Assert(bitIndex >= 0 && bitIndex < T);
			BitInfo bitInfo = .(bitIndex);
			let number = mBits[(int) bitInfo.ArrayIndex];
			return (number >> bitInfo.BitIndex) & 1UL == 1;
		}
		set mut
		{
			Runtime.Assert(bitIndex >= 0 && bitIndex < T);
			BitInfo bitInfo = .(bitIndex);
			if (value)
			{
				mBits[(int) bitInfo.ArrayIndex] |= bitInfo.BitMask;
			}
			else
			{
				mBits[(int) bitInfo.ArrayIndex] &= ~bitInfo.BitMask;
			}
		}
	}

	public bool Contains(BitVector<T> other)
	{
		for (int i < T)
		{
			if (this[i] == other[i])
			{
				return true;
			}
		}

		return false;
	}

	public static bool operator == (BitVector<T> lhs, BitVector<T> rhs)
	{
		for (int i < T)
		{
			if (lhs[i] && rhs[i])
			{
				continue;
			}
			else
			{
				return false;
			}
		}
		return true;
	}
}

#region Tests
static class BitVectorTests
{
	[Test]
	static void TestInitialisation()
	{
		BitVector<20> bv = .();
		Runtime.Assert([Friend]bv.mBits.Count == 1);
		bv[0] = true;
	}

	[Test(ShouldFail=true)]
	static void TestZeroInitialisation()
	{
		BitVector<0> bv = .();
		bv[0] = true; // Should fail
	}

	[Test]
	static void TestLargeInitialisation()
	{
		BitVector<200> bv = .();
		Test.Assert([Friend]bv.mBits.Count == 4);
	}

	[Test]
	static void TestSetBit()
	{
		BitVector<200> bv = .();
		bv[64] = true;
		Test.Assert(bv[64] == true);
	}

	[Test]
	static void TestSetLargeBit()
	{
		BitVector<500> bv = .();
		bv[420] = true;
		Test.Assert(bv[420] == true);
	}

	[Test]
	static void TestFlipBit()
	{
		BitVector<421> bv = .();
		bv[420] = true;
		Test.Assert(bv[420] == true);
		bv[420] = false;
		Test.Assert(bv[420] == false);
		bv[420] = true;
		Test.Assert(bv[420] == true);
	}

	[Test(ShouldFail=true)]
	static void TestOutOfBitVectorBounds()
	{
		BitVector<1> bv = .();
		bv[1] = true; // Should fail
	}
}
#endregion
