using System;
using System.Collections;
using System.Diagnostics;

namespace OverEngineeredPong.Collections;

public struct ArrayList<T, K> : IEnumerable<T>, IRefEnumerable<T*>
	where T : ValueType
	where K : const int
{
	T[K] mArray;
	int mHead = 0;

	public int Count => mHead;
	public T* Ptr => &mArray[0];

	public ref T this[int index]
	{
		get mut
		{
			Debug.Assert(index >= 0 && index < mArray.Count);
			return ref mArray[index];
		}
		set mut
		{
			Debug.Assert(index >= 0 && index < mArray.Count);
			mArray[index] = value;
		}
	}

	public void Append(T item) mut
	{
		if (mHead >= 0 && mHead < mArray.Count)
		{
			mArray[mHead] = item;
			mHead++;
		}
	}

	public void SwapRemove(int index) mut
	{
		if (index >= 0 && index < mArray.Count)
		{
			mArray[index] = mArray[mHead-1];
			mHead--;
		}
	}

	public struct Enumerator : IRefEnumerator<T*>, IEnumerator<T>
	{
		ArrayList<T, K>* mArrayList;
		int mIndex;
		T* mCurrent;

		public T Current
		{
		    get
		    {
		        return *mCurrent;
		    }
		}

		public ref T CurrentRef
		{
		    get
		    {
		        return ref *mCurrent;
		    }
		}

		public this(ArrayList<T, K>* arrayList)
		{
			mArrayList = arrayList;
			mIndex = 0;
			mCurrent = null;
		}

		public bool MoveNext() mut
		{
		    if ((uint(mIndex) < uint(mArrayList.Count)))
		    {
		        mCurrent = &mArrayList.Ptr[mIndex];
		        mIndex++;
		        return true;
		    }			   
		    return MoveNextRare();
		}

		private bool MoveNextRare() mut
		{
			mIndex = mArrayList.Count + 1;
		    mCurrent = null;
		    return false;
		}

		public Result<T*> GetNextRef() mut
		{
			if (!MoveNext())
				return .Err;
			return &CurrentRef;
		}

		public Result<T> GetNext() mut
		{
			if (!MoveNext())
				return .Err;
			return Current;
		}

		public void Remove() mut
		{
			int curIdx = mIndex - 1;
			mArrayList.SwapRemove(curIdx);
			mIndex = curIdx;
		}
	}

	public Enumerator GetEnumerator()
	{
		return Enumerator(&this);
	}
}


static class ArrayListTests
{
	[Test]
	static void EnumeratorRemoveTest()
	{
		ArrayList<int, 420> al = .();
		for (int i < 10)
		{
			al.Append(i);
		}

		for (var i in al)
		{
			if (i == 5)
				@i.Remove();
		}

		Test.Assert(al.Count == 9);
	}
}