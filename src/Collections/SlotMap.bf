using System;
using System.Collections;
using System.Diagnostics;

namespace OverEngineeredPong.Collections;

public struct Handle<T>
{
	public int Index { get; private set mut; }
	public int Generation { get; private set mut;  }

	public this(int index, int generation)
	{
		Index = index;
		Generation = generation;
	}
}

struct SlotMap<T, K> :
IEnumerable<T>, IRefEnumerable<T*>
	where T : ValueType
	where K : const int
{
	struct InternalHandle : this(int Index, int Generation);

	InternalHandle[K] mSlots;
	ArrayList<T, K> mData;

	int mFreeListHead;
	int mFreeListTail;
	int? mRecentlyInsertedSlotIndex;

	public this()
	{
		mSlots = .();
		mData = .();
		mRecentlyInsertedSlotIndex = null;

		mFreeListHead = 0;
		mFreeListTail = K-1;

		for (int i = 1; i < K; i++)
		{
			mSlots[i-1].Index = i;
		}

		mSlots[K-1].Index = K-1;
	}


	[NoDiscard]
	public Result<T*> Get(Handle<T> handle) mut
	{
		InternalHandle internalHandle = mSlots[handle.Index];
		if (internalHandle.Generation == handle.Generation)
		{
			return &mData[internalHandle.Index];
		}
		else
		{
			return .Err;
		}
	}

	public Result<void> Set(Handle<T> handle, T value) mut
	{
		InternalHandle internalHandle = mSlots[handle.Index];
		if (internalHandle.Generation == handle.Generation)
		{
			mData[internalHandle.Index] = value;
			return .Ok;
		}
		else
		{
			return .Err;
		}
	}

	[NoDiscard]
	public Handle<T> Insert(T data) mut
	{
		mRecentlyInsertedSlotIndex = mFreeListHead;
		InternalHandle* nextSlot = FreeListPop();

		mData.Append(data);
		nextSlot.Index = mData.Count - 1;

		Handle<T> handle = .(mRecentlyInsertedSlotIndex.Value, nextSlot.Generation);
		return handle;
	}

	public void Remove(Handle<T> handle) mut
	{
		if (handle.Generation == mSlots[handle.Index].Generation)
		{
			mSlots[handle.Index].Generation++;
			int dataIndex = mSlots[handle.Index].Index;
			mData.SwapRemove(dataIndex);
			mSlots[mRecentlyInsertedSlotIndex.Value].Index = dataIndex;
			FreeListPush(handle.Index);
		}
	}

	InternalHandle* FreeListPop() mut
	{
		Debug.Assert(mFreeListHead != mSlots[mFreeListHead].Index);
		InternalHandle* handle = &mSlots[mFreeListHead];
		mFreeListHead = handle.Index;

		return handle;
	}

	void FreeListPush(int index) mut
	{
		mSlots[mFreeListTail].Index = index;
		mFreeListTail = index;
	}

	public struct Enumerator : IEnumerator<T>, IRefEnumerator<T*>
	{
		ArrayList<T, K>.Enumerator mEnumerator;
		int mIndex;
		T* mCurrent;

		public ref T CurrentRef
		{
			get
			{
				return ref *mCurrent;
			}
		}

		public this(SlotMap<T, K> slotMap)
		{
			mEnumerator = slotMap.mData.GetEnumerator();
			mIndex = 0;
			mCurrent = null;
		}

		public Result<T*> GetNextRef() mut
		{
			return mEnumerator.GetNextRef();
		}

		public Result<T> GetNext() mut
		{
			return mEnumerator.GetNext();
		}
	}

	public Enumerator GetEnumerator()
	{
		return Enumerator(this);
	}
}

static class SlotMapTests
{
	struct Foo
	{
		public int X;
	}

	static SlotMap<Foo, 10> sTestSlotMap = .();

	[Test]
	static void IndexRefTest()
	{
		let handle = sTestSlotMap.Insert(Foo{X = 2});
		Foo* foo = sTestSlotMap.Get(handle);
		foo.X = 10;
		Test.Assert(sTestSlotMap.Get(handle).Value.X == 10);
	}

	[Test]
	static void PassingHandleToOtherFunc()
	{
		let handle = sTestSlotMap.Insert(Foo{X = 2});
		Foo* theFoo = FuncThatReturnsAFoo(handle);
		theFoo.X = 10;
		Test.Assert(sTestSlotMap.Get(handle).Value.X == 10);
	}

	static Result<Foo*> FuncThatReturnsAFoo(Handle<Foo> handle)
	{
		return sTestSlotMap.Get(handle);
	}
}
