using System;
using System.Reflection;
using System.Collections;

using OverEngineeredPong.Collections;
using OverEngineeredPong.ECS.Core;

namespace OverEngineeredPong;

// Hack! Can't comptime generate types
enum Registry : int64
{
	Greeting,
	Counter,
}

struct Counter : IComponent
{
	public int Count = 0;

	public void Dispose()
	{

	}
}

struct Greeting : IComponent
{
	public String Text;

	public void Dispose()
	{
		delete Text;
	}
}

[SystemSignature<Registry>(.Contains, Registry.Greeting)]
class SpawnerSystem : ISystem<Registry>
{
	const double mDelay = 1000;
	double mAccumulator = 0;

	public void Update(Scene<Registry> scene, double dt)
	{
		mAccumulator += dt;

		if (mAccumulator <= 1000) return;

		for (var entity in mEntities)
		{
			var greet = scene.GetGreetingComponent(entity);

			if (greet == .Err)
			{
				continue;
			}

			Console.WriteLine($"{greet.Value.Text}, again!");
		}

		mAccumulator = 0;
	}
}

[SystemSignature<Registry>(.Exact, Registry.Greeting, Registry.Counter)]
class GreetingSystem : ISystem<Registry>
{
	public void Update(Scene<Registry> scene, double dt)
	{
		for (var entity in mEntities)
		{
			var greet = scene.GetGreetingComponent(entity);
			var counter = scene.GetCounterComponent(entity);

			if (greet == .Err || counter == .Err)
			{
				continue;
			}
			
			Console.WriteLine(scope $"{counter.Value.Count}: {greet.Value.Text}");
			counter.Value.Count++;

			if (counter.Value.Count > 10)
			{
				scene.RemoveCounterComponent(&entity);
			}

		}
	}
}

class Program
{
	public static void Main(String[] args)
	{
		Scene<Registry> scene = new Scene<Registry>();
		defer delete scene;

		let e1 = scene.CreateEntity();
		scene.AddGreetingComponent(e1, Greeting { Text = "Hello" });
		scene.AddCounterComponent(e1, .());

		let e2 = scene.CreateEntity();
		scene.AddGreetingComponent(e2, Greeting { Text = "Bonjour" });
		scene.AddCounterComponent(e2, .());

		scene.AddSystem<GreetingSystem>();
		scene.AddSystem<SpawnerSystem>();

		scope AppBuilder<Registry>()
			.Init(WindowProps("Pong", 1280, 720))
			.WithScene(scene)
			.Run();
	}
}
