using SDL2;
using System;
using OpenGL;
using System.Collections;
using System.Diagnostics;
using OverEngineeredPong.ECS.Core;

namespace OverEngineeredPong;

struct WindowProps : this(StringView Title, int Width, int Height);

class AppBuilder<TRegistry> where TRegistry : enum
{
	static SDL.Window* sWindow;
	static Scene<TRegistry> sScene;

	public AppBuilder<TRegistry> Init(WindowProps props)
	{
		if (SDL.Init(.Everything) != 0)
		{
			SDL.Log(scope $"Unable to initialize SDL: {SDL.GetError()}");
			return null;
		}

		sWindow = SDL.CreateWindow("Pong", .Undefined, .Undefined, 800, 600, .OpenGL | .Shown);

		if (sWindow == null)
		{
			SDL.Log(scope $"Unable to create window: {SDL.GetError()}");
			return null;
		}

		SDL.GL_CreateContext(sWindow);
		GL.Init(=> SdlGetProcAddress);

		return this;
	}

	public AppBuilder<TRegistry> WithScene(Scene<TRegistry> scene)
	{
		sScene = scene;
		return this;
	}

	public void Run()
	{
		// TODO: Frame-independent game-loop
		bool running = true;
		double prevElapsedTime = 0;

		Stopwatch stopwatch = scope Stopwatch();
		stopwatch.Start();

		GameLoop:
		while (running)
		{
			SDL.Event event;
			while (SDL.PollEvent(out event) != 0)
			{
				if (event.type == .Quit)
				{
					break GameLoop;
				}
			}

			double elapsed = stopwatch.ElapsedMilliseconds;
			double dt = elapsed - prevElapsedTime;
			prevElapsedTime = elapsed;

			sScene.Update(dt);
			sScene.Draw();
		}
	}

	static void* SdlGetProcAddress(StringView string) {
		return SDL.SDL_GL_GetProcAddress(string.ToScopeCStr!());
	}
}
