using System;
using System.Diagnostics;
using System.Collections;
using OverEngineeredPong.Collections;

namespace OverEngineeredPong.ECS.Core;

public class Scene<TRegistry> where TRegistry : enum
{
	SlotMap<Entity<TRegistry>, 1024> mEntities;
	ComponentArrays<TRegistry, 1024> mComponents;
	Dictionary<uint64, Handle<Entity<TRegistry>>> mEntityHandles ~ delete _;
	List<ISystem<TRegistry>> mSystems = new List<ISystem<TRegistry>>() ~ DeleteContainerAndItems!(_);

	uint64 mNextId = 0;

	public this()
	{
		mEntities = .();
		mComponents = .();
		mEntityHandles = new Dictionary<uint64, Handle<Entity<TRegistry>>>();
	}

	public Handle<Entity<TRegistry>> CreateEntity()
	{
		let id = mNextId++;
		let handle = mEntities.Insert(Entity<TRegistry>(id));
		mEntityHandles.Add(id, handle);
		return handle;
	}

	public void AddSystem<TSystem>() where TSystem : ISystem<TRegistry>, new, class
	{
		var system = new TSystem();
		for (var entity in mEntities)
		{
			AddEntityToSystem(system, entity);
		}
		mSystems.Add(system);
	}

	public void RemoveSystem<K>() where K : ISystem<TRegistry>, new
	{
		var instanceOfK = scope K();

		for (var system in mSystems)
		{
			if (system.GetHashCode() == instanceOfK.GetHashCode())
			{
				@system.Remove();
			}
		}
	}

	public void Update(double dt)
	{
		for (let system in mSystems)
		{
			system.Update(this, dt);
		}
	}

	public void Draw()
	{
		// Kick off renderer.
	}

	void AddEntityToSystem(ISystem<TRegistry> system, Entity<TRegistry> entity)
	{
		var entity;
		switch (system.Match)
		{
		case .Exact:
			if (entity.Signature == system.Signature)
			{
				system.AddEntity(entity);
			}
			break;
		case .Contains:
			if (entity.Signature.Contains(system.Signature))
			{
				system.AddEntity(entity);
			}
			break;
		}
	}

	void RemoveEntityFromSystem(ISystem<TRegistry> system, Entity<TRegistry> entity)
	{
		var entity;
		switch (system.Match)
		{
		case .Exact:
			if (entity.Signature == system.Signature)
			{
				system.RemoveEntity(entity.Id);
			}
			break;
		case .Contains:
			if (!entity.Signature.Contains(system.Signature))
			{
				system.RemoveEntity(entity.Id);
			}
			break;
		}
	}

	void UpdateEntityInSystems(ISystem<TRegistry> system, Entity<TRegistry> entity)
	{
		var entity;
		switch (system.Match)
		{
		case .Exact:
			if (entity.Signature == system.Signature)
			{
				system.AddEntity(entity);
			}
			else
			{
				system.RemoveEntity(entity.Id);
			}
			break;
		case .Contains:
			if (entity.Signature.Contains(system.Signature))
			{
				system.AddEntity(entity);
			}
			else
			{
				system.RemoveEntity(entity.Id);
			}
			break;
		}
	}

	[Comptime, OnCompile(.TypeInit)]
	static void GenerateMethods()
	{
		String typeName = scope String();
		typeof(TRegistry).GetName(typeName);
		String hopeThisWorks = scope String();

		for (let fieldInfo in typeof(TRegistry).GetFields())
		{
			StringView componentName = fieldInfo.Name;
			hopeThisWorks.Append(scope $"\tmComponents.RemoveComponent(Handle<{componentName}>(component.Index, component.Generation));\n");

			Compiler.EmitTypeBody(typeof(Self), scope $"""
				public Result<{componentName}*> Get{componentName}Component(Handle<Entity<TRegistry>> entityHandle) mut
				{{
					Entity<TRegistry>* entity = mEntities.Get(entityHandle);
					Handle<{componentName}> handle = entity.Get{componentName}ComponentHandle();
					return mComponents.GetComponent(handle);
				}}

				public Result<{componentName}*> Get{componentName}Component(Entity<TRegistry> entity) mut
				{{
					var entity;
					Handle<{componentName}> handle = entity.Get{componentName}ComponentHandle();
					return mComponents.GetComponent(handle);
				}}

				public void Add{componentName}Component(Handle<Entity<TRegistry>> entityHandle, {componentName} component) mut
				{{
					Entity<TRegistry>* entity = mEntities.Get(entityHandle);
					Add{componentName}Component(entity, component);
				}}

				public void Add{componentName}Component(Entity<TRegistry>* entity, {componentName} component) mut
				{{
					entity.Signature.AddComponent({typeName}.{componentName});
					Handle<{componentName}> typedHandle = mComponents.AddComponent(component);
					entity.ComponentHandles[(int) {typeName}.{componentName}] = .(typedHandle.Index, typedHandle.Generation);
	
					for (var system in mSystems)
					{{
						UpdateEntityInSystems(system, *entity);
					}}
				}}

				public void Remove{componentName}Component(Handle<Entity<TRegistry>> entityHandle) mut
				{{
					Entity<TRegistry>* entity = mEntities.Get(entityHandle);
					Handle<{componentName}> handleToRemove = entity.Get{componentName}ComponentHandle();
					mComponents.RemoveComponent(handleToRemove);
					entity.Signature.RemoveComponent(.{componentName});

					for (var system in mSystems)
					{{
						RemoveEntityFromSystem(system, entity.Id);
					}}
				}}

				public void Remove{componentName}Component(Entity<TRegistry>* entity) mut
				{{
					Handle<{componentName}> handleToRemove = entity.Get{componentName}ComponentHandle();
					mComponents.RemoveComponent(handleToRemove);
					entity.Signature.RemoveComponent(.{componentName});

					for (var system in mSystems)
					{{
						RemoveEntityFromSystem(system, *entity);
					}}
				}}

				""");
		}

		Compiler.EmitTypeBody(typeof(Self), scope $"""
			public void DeleteEntity(Entity<TRegistry>* entityToDelete)
			{{
				Debug.Assert(mEntityHandles.ContainsKey(entityToDelete.Id));
				let handle = mEntityHandles[entityToDelete.Id];

				for (var system in mSystems)
				{{
					RemoveEntityFromSystem(system, *entityToDelete);
				}}

				for (var component in entityToDelete.ComponentHandles)
				{{
				{hopeThisWorks}
				}}

				mEntities.Remove(handle);
				mEntityHandles.Remove(entityToDelete.Id);

			}}
			""");
	}
	
}
