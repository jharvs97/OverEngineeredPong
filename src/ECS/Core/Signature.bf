using System;
using OverEngineeredPong.Collections;

namespace OverEngineeredPong.ECS.Core;

public struct ComponentSignature<TRegistry> where TRegistry : enum
{
	[Comptime, OnCompile(.TypeInit)]
	static void GenerateBitVector()
	{
		let numBits = (int) typeof(TRegistry).MaxValue + 1;
		Compiler.EmitTypeBody(typeof(Self), scope $"""
			BitVector<{numBits}> mBitVector = BitVector<{numBits}>();
			""");
	}

	public this(params TRegistry[] components)
	{
		for (let component in components)
		{
			mBitVector[ComponentToIndex(component)] = true;
		}
	}

	[Inline]
	public bool HasComponent(TRegistry component)
	{
		int index = ComponentToIndex(component);
		return mBitVector[index];
	}

	[Inline]
	public void AddComponent(TRegistry component) mut
	{
		let index = ComponentToIndex(component);
		mBitVector[index] = true;
	}

	[Inline]
	public void RemoveComponent(TRegistry component) mut
	{
		let index = ComponentToIndex(component);
		mBitVector[index] = true;
	}

	[Inline]
	int ComponentToIndex(TRegistry component)
	{
		return (int) component;
	}

	[Inline]
	public bool Contains(ComponentSignature<TRegistry> other)
	{
		return this.mBitVector.Contains(other.mBitVector);
	}

	public static bool operator ==(ComponentSignature<TRegistry> lhs, ComponentSignature<TRegistry> rhs)
	{
		return lhs.mBitVector == rhs.mBitVector;
	}	
}
