using System;
using System.Reflection;
using System.Collections;

namespace OverEngineeredPong.ECS.Core;

interface ISystem<TRegistry> : IHashable where TRegistry : enum
{
	ref ComponentSignature<TRegistry> Signature { get mut; }
	SignatureMatch Match { get; }

	void Update(Scene<TRegistry> scene, double dt);
	void AddEntity(Entity<TRegistry> entity);
	void RemoveEntity(uint64 id);
}

public enum SignatureMatch
{
	Contains,
	Exact,
}

[AttributeUsage(.Class)]
public struct SystemSignatureAttribute<TRegistry> : Attribute, IOnTypeInit where TRegistry : enum
{
	const int mMaxFlags = (int) typeof(TRegistry).MaxValue + 1;
	TRegistry[mMaxFlags] mComponentFlags = .();
	int mCount = 0;
	SignatureMatch mMatch = .Exact;

	public this(SignatureMatch match, params TRegistry[] components)
	{
		int i = 0;
		for (let component in components)
		{
			mComponentFlags[i] = component;
			i++;
		}
		mCount = i;
		mMatch = match;
	}

	[Comptime]
	public void OnTypeInit(Type type, Self* prev)
	{
		String systemName = scope String();
		type.GetName(systemName);

		String componentEnumName = scope String();
		typeof(TRegistry).GetName(componentEnumName);

		String components = scope String();

		if (mCount > 0)
		{
			for (int i < mCount - 1)
			{
				components.Append(scope $".{mComponentFlags[i]}, ");
			}

			components.Append(scope $".{mComponentFlags[mCount - 1]}");
		}

		String matchString = scope String();
		mMatch.ToString(matchString);

		Compiler.EmitTypeBody(type, scope $"""
			ComponentSignature<{componentEnumName}> mSignature = .({components});
			public ref ComponentSignature<{componentEnumName}> Signature => ref mSignature;

			List<Entity<{componentEnumName}>> mEntities = new List<Entity<{componentEnumName}>>() ~ delete _;

			public SignatureMatch Match => SignatureMatch.{matchString};

			public int GetHashCode()
			{{
				return GetSystemHashCode();
			}}

			public static int GetSystemHashCode()
			{{
				return {systemName.GetHashCode()};
			}}

			public void AddEntity(Entity<{componentEnumName}> entity)
			{{
				mEntities.Add(entity);
			}}

			public void RemoveEntity(uint64 id)
			{{
				for (var entity in mEntities)
				{{
					if (entity.Id == id)
					{{
						@entity.Remove();
						break;
					}}
				}}
			}}
			""");
	}
}
