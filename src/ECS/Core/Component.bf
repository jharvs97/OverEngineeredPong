using System;
using System.Collections;

namespace OverEngineeredPong.ECS.Core;

public struct OpaqueComponentHandle
{
	public int Index { get; private set mut; }
	public int Generation { get; private set mut;  }

	public this(int index, int generation)
	{
		Index = index;
		Generation = generation;
	}
}

public interface IComponent : IDisposable
{

}
