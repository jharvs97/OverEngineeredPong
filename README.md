WIP Pong game with custom ECS architecture, leveraging Beefs 'Comptime' capabilities.

To build, you will need Beef OpenGL bindings and the tinyAlgebra library.