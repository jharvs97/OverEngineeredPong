using System;
using OverEngineeredPong.Collections;

namespace OverEngineeredPong.ECS.Core;

public struct ComponentArrays<TRegistry, ArraySize>
	where TRegistry : enum
	where ArraySize : const int
{
	[Comptime, OnCompile(.TypeInit)]
	static void GenerateArrays()
	{
		for (let fieldInfo in typeof(TRegistry).GetFields())
		{
			int numComponents = ArraySize;
			StringView component = fieldInfo.Name;
			Compiler.EmitTypeBody(typeof(Self), scope $"""
				public SlotMap<{component}, {numComponents}> m{component}Array = SlotMap<{component}, {numComponents}>();

				public Result<{component}*> GetComponent(Handle<{component}> handle) mut
				{{
					return m{component}Array.Get(handle);
				}}

				[NoDiscard]
				public Handle<{component}> AddComponent({component} data) mut
				{{
					return m{component}Array.Insert(data);
				}}

				public void RemoveComponent(Handle<{component}> toRemove) mut
				{{
					m{component}Array.Remove(toRemove);
				}}

				""");
		}
	}
}
