using System;
using OverEngineeredPong.Collections;

namespace OverEngineeredPong.ECS.Core;

public struct Entity<TRegistry> where TRegistry : enum
{
	uint64 mId;
	public uint64 Id => mId;

	ComponentSignature<TRegistry> mSignature;
	public ref ComponentSignature<TRegistry> Signature mut => ref mSignature;

	const int mMaxComponents = (int) typeof(TRegistry).MaxValue + 1;
	OpaqueComponentHandle[mMaxComponents] mComponentHandles;

	public ref OpaqueComponentHandle[mMaxComponents] ComponentHandles mut => ref mComponentHandles;

	public this(uint64 id)
	{
		mId = id;
		mSignature = .();
		mComponentHandles = .();
	}

	[Comptime, OnCompile(.TypeInit)]
	static void GenerateMethods()
	{
		for (let fieldInfo in typeof(TRegistry).GetFields())
		{
			StringView name = fieldInfo.Name;
			String typeName = scope String();
			typeof(TRegistry).GetName(typeName);

			Compiler.EmitTypeBody(typeof(Self), scope $"""
				public Handle<{name}> Get{name}ComponentHandle() mut
				{{
					Runtime.Assert(Signature.HasComponent(.{name}));
					let handle = mComponentHandles[(int) {typeName }.{name}];
					return Handle<{name}>(handle.Index, handle.Generation);
				}}\n
				""");
		}
	}
}

static class EntityTests
{
	enum Components
	{
		Foo, Bar
	}

	[Test]
	static void SetSignature()
	{
		Entity<Components> e = .(0);
		e.Signature.AddComponent(.Foo);
		Runtime.Assert(e.Signature.HasComponent(.Foo));
	}
}
